Feature: Smoke
  As a user
  I want to test all main site functionality
  So that I can be sure that site works correctly
Scenario Outline: Check site sign in and registration  functions
  Given User opens '<homePage>' page
  And User checks header visibility
  And User checks footer visibility
  When User move to my account icon
  And User checks sign in button visibility
  And User clicks sign in button
  And User checks email visibility
  And User checks password visibility
  And User checks sign in button  visibility
  And User clicks registration button in signIn page
  And User checks form registration visibility

  Examples:
    | homePage                            |
    | https://www.asos.com/ru/|
  Scenario Outline: Check fit analytics functionality
    Given User opens '<homePage>' page
    And User checks search field visibility
    When User makes search by keyword '<keyword>'
    And User clicks search button
    And User checks fit analytics button visibility
    And User clicks fit analytics button
    Then User check and enter his data '<sm>' and '<kg>'
    And User checks radio buttons <first> and <second>
    And User enter his age '<age>'


    Examples:
      | homePage                            |keyword|sm|kg|first|second|age|
      | https://www.asos.com/ru/men/|chicken            |162|56|0|2    |22 |

  Scenario Outline: Check log out button

    Given User opens '<homePage>' page
    And User checks header visibility
    And User checks footer visibility
    When User move to my account icon
    And User checks sign in button visibility
    And User clicks sign in button
    And User checks email in sign in visibility
    When User enter email '<email>'
    When User enter password '<password>'
    And User clicks sign in button on signInPage
    When User move to my account icon
    And User checks name account visibility
    And User checks name of account is '<name>'
    And User clicks on log out button

    Examples:
      | homePage                |email             |password   |name|
      | https://www.asos.com/ru/|torynyan@gmail.com|ZYtpyf.6789|Victoria|

  Scenario Outline: Check checkout  functions
    Given User opens '<homePage>' page
    And User checks header visibility
    And User checks footer visibility

    And User checks search field visibility
    When User makes search by keyword '<keyword>'
    And User clicks search button

    And User clicks 'Add to Cart' button on product
    And User checks that add to cart popup visible
    And User checks 'Checkout' button visibility
    And User checks 'Continue to Cart' button visibility

    And User clicks 'Checkout' button
    And User checks email in sign in visibility
    When User enter email '<email>'
    When User enter password '<password>'
    And User clicks sign in button on signInPage
    And User checks Error message on ProductPage

    Examples:
      | homePage                |email             |password   | keyword|
      | https://www.asos.com/ru/|torynyan@gmail.com|ZYtpyf.6789|24466592   |

  Scenario Outline: Check registration with valid data
  Given User opens '<homePage>' page
  When User move to my account icon
  And User checks register button visibility
  And User clicks registration button
  And User checks form registration visibility
  When User enter email form '<emailForm>'
  When User enter password form '<passwordForm>'
  When User enter first name '<firstNameForm>'

  And User select birthday form
  When User enter last name '<lastNameForm>'
  And User click sign up on Asos button
  When User move to my account icon
  And User checks name account visibility
  And User checks name of account is '<name>'

  Examples:
    | homePage                |emailForm          |passwordForm   |firstNameForm|lastNameForm|name|
    | https://www.asos.com/ru/|ygch@gmail.com|abcdefu12345|Victoria|Boldyrieva            |Victoria |

  Scenario Outline: Check registration with invalid data of email
    Given User opens '<homePage>' page
    When User move to my account icon
    And User checks register button visibility
    And User clicks registration button
    And User checks form registration visibility
    When User enter email form '<emailForm>'
    When User enter password form '<passwordForm>'
    When User enter first name '<firstNameForm>'

    And User select birthday form
    When User enter last name '<lastNameForm>'
    And User click sign up on Asos button
    And User checks Error message '<error>'

    Examples:
      | homePage                |emailForm          |passwordForm   |firstNameForm|lastNameForm|error|
      | https://www.asos.com/ru/|ygchgmail.com|abcdefu12345|Victoria|Boldyrieva            |Введенный адрес электронной почты не верен! |

  Scenario Outline: Check sign in with valid data

    Given User opens '<homePage>' page
    And User checks header visibility
    And User checks footer visibility
    When User move to my account icon
    And User checks sign in button visibility
    And User clicks sign in button
    When User enter email '<email>'
    When User enter password '<password>'
    And User clicks sign in button on signInPage
    When User move to my account icon
    And User checks name account visibility
    And User checks name of account is '<name>'

    Examples:
      | homePage                |email             |password   |name|
      | https://www.asos.com/ru/|torynyan@gmail.com|ZYtpyf.6789|Victoria|

  Scenario Outline: Check site main functions
    Given User opens '<homePage>' page
    And User checks header visibility
    And User checks footer visibility
    And User checks search field visibility
    And User checks cart visibility
    And User checks that marketplace visible
    And User opens language popup
    And User checks that language popup visible
    And User checks that language close popup button visible
    And User closes language popup
    When User move to my account icon
    And User checks sign in button visibility
    And User checks register button visibility
    And User opens marketplace
    Then User checks marketplace title visible

    Examples:
      | homePage                            |
      | https://www.asos.com/ru/            |

  Scenario Outline: Check add product to cart
    Given User opens '<homePage>' page
    And User checks search field visibility
    When User makes search by keyword '<keyword>'
    And User clicks search button

    And User clicks 'Add to Cart' button on product
    And User checks that add to cart popup visible
    And User checks 'Checkout' button visibility
    And User checks 'Continue to Cart' button visibility
    Then User checks that add to cart popup header is '<header>'
    And User clicks 'Checkout' button


    Examples:
      | homePage                            | keyword  | header  |
      | https://www.asos.com/ru/  | 24466592 | 1 товар |

  Scenario Outline: Check ERROR of add product to wishlist
    Given User opens '<homePage>' page
    When User move to my account icon
    And User checks sign in button visibility
    And User clicks sign in button
    And User checks email visibility
    When User enter email '<email>'
    When User enter password '<password>'
    And User clicks sign in button on signInPage
    And User checks cart visibility
    And User checks search field visibility
    When User makes search by keyword '<keyword>'
    And User clicks search button
    And User clicks wish list on first product
    Then User checks wishListPage does not work and sees error message '<errorMessage>'

    Examples:
      | homePage                            |email             |password   |keyword| errorMessage |
      | https://www.asos.com/ru/|torynyan@gmail.com|ZYtpyf.6789| new balance              |Ошибка                |
