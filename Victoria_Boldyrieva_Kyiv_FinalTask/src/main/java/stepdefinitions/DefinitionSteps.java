package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.*;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static java.lang.Thread.sleep;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class DefinitionSteps {

    private static final long DEFAULT_TIMEOUT = 120;

    WebDriver driver;
    HomePage homePage;
    ShoppingCartPage shoppingCartPage;
    MarketPlacePage marketPlacePage;
    SearchResultPage searchResultPage;
    ProductPage productPage;
    SignInPage signInPage;
    WishListPage wishListPage;
    RegisterPage registerPage;
//    CheckoutPage checkoutPage;
    PageFactoryManager pageFactoryManager;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @And("User opens {string} page")
    public void openPage(final String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);
    }

    @And("User checks header visibility")
    public void userChecksHeaderVisibility() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        homePage.isHeaderVisible();
    }

    @And("User checks footer visibility")
    public void userChecksFooterVisibility() {
        homePage.isFooterVisible();
    }

    @And("User checks search field visibility")
    public void userChecksSearchFieldVisibility() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.isSearchFieldVisible();
    }

    @And("User checks cart visibility")
    public void userChecksCartVisibility() {
        homePage.isCartIconVisible();
    }



    @When("User move to my account icon")
    public void userMoveToMyAccountIcon()  {
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        homePage.moveToMyAccountIcon();

    }
    @And("User checks register button visibility")
    public void userChecksRegisterButtonVisibility() {

        homePage.isRegisterButtonVisible();
    }

    @And("User checks sign in button visibility")
    public void userChecksSignInButtonVisibility() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT,homePage.getSignInButton());
    }

    @And("User checks that marketplace visible")
    public void userChecksThatMarketplaceVisible() {
        homePage.isMarketPlaceVisible();
    }
    @And("User opens marketplace")
    public void userOpensStorePopup() {
        homePage.clickMarketPlaceButton();
        marketPlacePage = pageFactoryManager.getMarketPlacePage();
       marketPlacePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        marketPlacePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        marketPlacePage.waitVisibilityOfElement(DEFAULT_TIMEOUT,marketPlacePage.getMarketplaceTitle());
    }



    @And("User opens shopping cart")
    public void userOpensShoppingCart() {
        homePage.clickCartButton();
        shoppingCartPage = pageFactoryManager.getShoppingCartPage();
        shoppingCartPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        shoppingCartPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        shoppingCartPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,shoppingCartPage.getShoppingCartTitle());
    }

    @Then("User checks that shopping cart title visible")
    public void userChecksThatShoppingCartTitleVisible() {
        assertTrue(shoppingCartPage.isShoppingCartTitleVisible());
    }
    @After
    public void tearDown() {
        driver.close();
    }


    @And("User opens language popup")
    public void userOpensLanguagePopup() {
        homePage.clickLanguageButton();
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT,homePage.getLanguagePopup());
    }

    @And("User checks that language popup visible")
    public void userChecksThatLanguagePopupVisible() {
        homePage.isLanguagePopupVisible();
    }

    @And("User checks that language close popup button visible")
    public void userChecksThatLanguageClosePopupButtonVisible() {
        homePage.isLanguageClosePopupButtonVisible();
    }

    @And("User closes language popup")
    public void userClosesLanguagePopup() {
        homePage.clickLanguageClosePopupButton();
    }

    @And("User checks marketplace title visible")
    public void userChecksMarketplaceTitleVisible() {
        marketPlacePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        marketPlacePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
       assertTrue(marketPlacePage.isMarketPlaceTitleVisible());
    }

    @And("User change language to Poland")
    public void userChangeLanguageToPoland() {
homePage.clickLanguageButton();
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT,homePage.getLanguagePopup());
        homePage.clickLanguageDropDown();
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT,homePage.getLanguageDropDown());
        homePage.clickLanguagePoland();
        homePage.clickSaveCountryButton();
    }

    @Then("User checks that current url contains {string} language")
    public void userChecksThatCurrentUrlContainsFlLanguage(final String language) {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertTrue(driver.getCurrentUrl().contains(language));
    }

    @When("User makes search by keyword {string}")
    public void userMakesSearchByKeywordKeyword(final String keyword) {
        homePage.enterTextToSearchField(keyword);
    }

    @And("User clicks search button")
    public void userClicksSearchButton()  {
        homePage.clickSearchButton();
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        searchResultPage = pageFactoryManager.getSearchResultPage();

        searchResultPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        searchResultPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);

    }

    @And("User clicks 'Add to Cart' button on product")
    public void userClicksAddToCartButtonOnProduct() {
        productPage = pageFactoryManager.getProductPage();
        productPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        productPage.waitForAjaxToCompletePdp(DEFAULT_TIMEOUT);
        productPage.clickAddToCartButton();
    }

    @And("User checks that add to cart popup visible")
    public void userChecksThatAddToCartPopupVisible() {
        productPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, productPage.getAddToCartPopupHeader());
        assertTrue(productPage.isAddToCartPopupVisible());
    }

    @And("User checks 'Checkout' button visibility")
    public void userChecksContinueShoppingButtonVisibility() {
        productPage.isCheckoutButtonVisible();
    }

    @And("User checks 'Continue to Cart' button visibility")
    public void checkContinueToCartButtonVisibility() {
        productPage.isContinueToCartButtonVisible();
    }

    @Then("User checks that add to cart popup header is {string}")
    public void userChecksThatAddToCartPopupHeaderIsHeader(final String expectedText) {
        assertEquals(productPage.getAddToCartPopupHeaderText(), expectedText);

    }

    @And("User clicks 'Checkout' button")
    public void userClicksContinueToCartButton() {
        productPage.clickCheckoutButton();
        signInPage = pageFactoryManager.getSignInPage();
        signInPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,signInPage.getSignInTitle());
    }


    @And("User clicks sign in button")
    public void userClicksSignInButton() {
        homePage.clickSignInButton();
        signInPage = pageFactoryManager.getSignInPage();
        signInPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,signInPage.getSignInTitle());

    }

    @And("User checks email visibility")
    public void userChecksEmailVisibility() {
        signInPage.isEmailFieldVisible();
    }

    @And("User checks password visibility")
    public void userChecksPasswordVisibility() {
        signInPage.isPasswordFieldVisible();
    }

    @And("User checks sign in button  visibility")
    public void checksSignInButtonVisibility() {
        signInPage.isSignInButtonVisible();
    }

    @And("User clicks registration button in signIn page")
    public void userClicksRegistrationButton() {
        signInPage.clickRegistrationBlank();
        signInPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User checks form registration visibility")
    public void userChecksFormRegistrationVisibility() {
        registerPage = pageFactoryManager.getRegisterPage();
        registerPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        registerPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        registerPage.isRegistrationFormVisible();
    }

    @When("User enter email {string}")
    public void userEnterEmailEmail(final String email) {
        signInPage.enterEmailToEmailField(email);

    }

    @When("User enter password {string}")
    public void userEnterPasswordPassword(final String password) {
        signInPage.enterPasswordToPasswordField(password);    }

    @And("User clicks sign in button on signInPage")
    public void userClicksSignInButtonOnSignInPage()  {
        signInPage.clickSignInButton();

        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);

    }

    @And("User checks name of account is {string}")
    public void userChecksNameOfAccountIsName(final String name) {
        assertTrue(homePage.getNameAccountText().contains(name));
    }

    @And("User checks name account visibility")
    public void userChecksNameAccountVisibility() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT,homePage.getNameAccount());
    }

    @And("User clicks registration button")
    public void clicksRegistrationButton() {
        homePage.clickRegisterButton();
        registerPage = pageFactoryManager.getRegisterPage();
        registerPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,registerPage.getRegisterForm());
    }

    @And("User select birthday form")
    public void userSelectBirthdayForm()  {

        registerPage.selectDayForm();
        registerPage.selectMonthForm();

        registerPage.selectYearForm();

    }

    @When("User enter first name {string}")
    public void userEnterFirstNameFirstNameForm(final String firstName) {
        registerPage.enterFirstNameToFirstNameForm(firstName);
    }


    @When("User enter last name {string}")
    public void userEnterLastNameLastNameForm(final String lastName) {
        registerPage.enterLastNameToLastNameForm(lastName);

    }

    @And("User click sign up on Asos button")
    public void userClickSignUpOnAsosButton()  {
        registerPage.clickSignUpAsosButton();



    }
    @And("User clicks wish list on first product")
    public void clickWishList() {

        searchResultPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        searchResultPage.clickWishListOnFirstProduct();
    }
    @And("User checks wishListPage does not work and sees error message {string}")
    public void userChecksWishListPageDoesNotWorkAndSeesErrorMessage(final String errorMessage) {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getWishListProductsCount());
        homePage.clickWishListProductsCount();
        wishListPage = pageFactoryManager.getWishListPage();
        wishListPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        wishListPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        wishListPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,wishListPage.getError());

        assertTrue(wishListPage.getErrorText().contains(errorMessage));
    }

    @And("User checks email in sign in visibility")
    public void userChecksEmailInSignInVisibility() {

    }

    @When("User enter email form {string}")
    public void userEnterEmailFormEmailForm(final String emailForm) {
        registerPage.enterEmailToEmailForm(emailForm);
    }

    @When("User enter password form {string}")
    public void userEnterPasswordFormPasswordForm(final String passwordForm) {
   registerPage.enterPasswordToPasswordForm(passwordForm);
    }

    @And("User checks Error message {string}")
    public void userChecksErrorMessageErrorMessage(final String errorMessage) {
        productPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,productPage.getErrorText());
    }


    @And("User select product size")
    public void userSelectProductSize() {
        productPage.selectProduct();
    }

    @And("User checks Error message on ProductPage")
    public void userChecksErrorMessageOnProductPage() {
        productPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,productPage.getErrorText());
    }


    @And("User clicks on log out button")
    public void userClicksOnLogOutButton() {
        homePage.clickLogOutButton();
    }

    @And("user clicks on aboutUsButton;")
    public void userClicksOnAboutUsButton() {
        homePage.clickAboutUsButton();
    }

    @And("User checks fit analytics button visibility")
    public void userChecksFitAnalyticsButtonVisibility()  {
        productPage = pageFactoryManager.getProductPage();
        productPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        productPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);

        productPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,productPage.getFitAnalyticsButton());
    }

    @And("User clicks fit analytics button")
    public void userClicksFitAnalyticsButton()  {
        productPage.clickFitAnalyticsButton();
        productPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,productPage.getFitAnalyticsPopup());
    }

    @Then("User check and enter his data {string} and {string}")
    public void userCheckAndEnterHisDataSmAndKg(final String sm, final String kg)  {
productPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,productPage.getFitSM());
        productPage.enterFitSM(sm);
        productPage.enterFitKG(kg);
        productPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        productPage.waitForAjaxToCompletePdp(DEFAULT_TIMEOUT);


    }

    @And("User checks radio buttons {int} and {int}")
    public void userChecksRadioButtonsFirstAndSecond(final int first, final int second) throws InterruptedException {
        productPage.clickFitContinueButton();
        productPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        productPage.waitForAjaxToCompletePdp(DEFAULT_TIMEOUT);
        productPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,productPage.getFitPopupTitle());

        productPage.clickRadioButton(first);

        productPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,productPage.getFitContinueButton());

        productPage.clickFitContinueButton();
        productPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,productPage.getFitPopupTitle());
        productPage.clickRadioButton(second);

        productPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,productPage.getFitContinueButton());

        productPage.clickFitContinueButton();
        productPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,productPage.getFitPopupTitle());
    }

    @And("User enter his age {string}")
    public void userEnterHisAgeAge(final String age) {
        productPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,productPage.getFitPopupTitle());
//        sleep(3000);
        productPage.enterFitKG(age);

        productPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,productPage.getFitContinueButton());

        productPage.clickFitContinueButton();
    }
}
