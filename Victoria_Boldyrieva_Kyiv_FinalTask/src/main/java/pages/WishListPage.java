package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WishListPage extends BasePage{
    @FindBy(xpath = "//div[@class='pageLoadError_3s58N']")
    private WebElement pageLoadError;
    @FindBy(xpath = "//h2[@class='title_26Pyp']")
    private WebElement textOfError;
    public WishListPage(WebDriver driver) {
        super(driver);
    }


    public WebElement getError() {
        return pageLoadError;
    }

    public String getErrorText(){return textOfError.getText();}


}
