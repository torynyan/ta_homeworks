package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class ProductPage extends BasePage{

    @FindBy(xpath = "//button[@data-test-id='add-button']")
    private WebElement addToCartButton;

    @FindBy(xpath = "//span[@data-test-id='miniBagItemCount']")
    private WebElement addToCartPopupHeader;

    @FindBy(xpath = "//a[@data-test-id='checkout-link']")
    private WebElement checkoutButton;

    @FindBy(xpath = "//a[@data-test-id='bag-link']")
    private WebElement continueToCartButton;

    @FindBy(xpath = "//select[//select[@data-id='sizeSelect']]")
    private WebElement productDropDown;
    Select selectProduct = new Select(productDropDown);

    @FindBy(xpath = "//div[@class='error-overlay is-failed']")
    private WebElement errorText;

 @FindBy(xpath = "//button[@class='link fit-lower']")
 private WebElement fitAnalyticsButton;

    @FindBy(xpath = "//div[@class='uclw_whitebox_container']")
    private WebElement fitAnalyticsPopup;
    @FindBy(xpath = "//input[@aria-label='Рост в сантиметрах']")
    private WebElement fitSM;
    @FindBy(xpath = "//input[@aria-label='Вес в килограммах']")
    private WebElement fitKG;
    @FindBy(xpath = "//button[text()='Продолжить']")
    private WebElement fitContinueButton;
    @FindBy(xpath = "//li[contains(@class,'uclw_item ')]")
    private List<WebElement> fitRadioButtons;
    @FindBy(xpath = "//div[@class='uclw_headline']")
    private WebElement fitPopupTitle;
    @FindBy(xpath = "//input[@class='uclw_input_text']")
    private WebElement fitAge;
//    @FindBy(xpath = "//div[@class='uclw_whitebox_container']")
//    private WebElement fitAnalyticsPopup;
//    @FindBy(xpath = "//div[@class='uclw_whitebox_container']")
//    private WebElement fitAnalyticsPopup;

    public ProductPage(WebDriver driver) {
        super(driver);
    }

    public void clickAddToCartButton() {
      addToCartButton.click();
    }

    public boolean isAddToCartPopupVisible() {
        return addToCartPopupHeader.isDisplayed();
    }

    public void isCheckoutButtonVisible() {
        checkoutButton.isDisplayed();
    }

    public String getAddToCartPopupHeaderText() {
        return addToCartPopupHeader.getText();
    }

    public void isContinueToCartButtonVisible() {
        continueToCartButton.isDisplayed();
    }

    public void clickCheckoutButton() {
        checkoutButton.click();
    }

    public WebElement getAddToCartPopupHeader() {
        return addToCartPopupHeader;
    }

    public void selectProduct() {
        selectProduct.selectByValue("S - Chest 38");
    }

    public WebElement getErrorText() {
        return errorText;
    }

    public WebElement getFitAnalyticsButton() {return fitAnalyticsButton;}

    public void clickFitAnalyticsButton(){ fitAnalyticsButton.click();}

    public WebElement getFitAnalyticsPopup() {return fitAnalyticsPopup;}

    public WebElement getFitSM(){return fitSM;}

    public void enterFitSM(final String sm) {
        fitSM.clear();
        fitSM.sendKeys(sm);
    }

    public void enterFitKG(final String kg) {
        fitKG.clear();
        fitKG.sendKeys(kg);
    }

    public void clickFitContinueButton(){fitContinueButton.click();}

    public WebElement getFitContinueButton(){return fitContinueButton;}

    //public WebElement getRadioButton(){return fitRadioButtons.get(0);}

    public void clickRadioButton(int ind) {fitRadioButtons.get(ind).click();
    }

    public WebElement getFitPopupTitle(){return fitPopupTitle;}

    public void enterFitAge(final String age) {
        fitAge.clear();
        fitAge.sendKeys(age);
    }

    public void setFitRadioButtons(List<WebElement> fitRadioButtons) {
        this.fitRadioButtons = fitRadioButtons;
    }
}


