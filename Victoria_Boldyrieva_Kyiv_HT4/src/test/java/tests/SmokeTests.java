package tests;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SmokeTests extends BaseTest {

    private static final long DEFAULT_WAITING_TIME = 90;


    @Test
    public void checkMainComponentsOnHomePage() {
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        assertTrue(getHomePage().isHeaderVisible());
        assertTrue(getHomePage().isFooterVisible());
        assertTrue(getHomePage().isCartIconVisible());
        assertTrue(getHomePage().isSearchFieldVisible());
        assertTrue(getHomePage().isSearchButtonVisible());
        assertTrue(getHomePage().isMyAccountButtonVisible());
        assertTrue(getHomePage().isCallBackButtonVisible());


        getHomePage().clickMyAccountButton();
        getHomePage().waitVisibilityOfElement(DEFAULT_WAITING_TIME, getHomePage().getMyAccountPopup());
        assertTrue(getHomePage().isEmailFieldVisible());
        assertTrue(getHomePage().isPasswordFieldVisible());
        assertTrue(getHomePage().isSignInVisible());
        assertTrue(getHomePage().isRegisterVisible());

        getHomePage().clickCallBackButton();
        getHomePage().waitVisibilityOfElement(DEFAULT_WAITING_TIME, getHomePage().getCallBackPopup());
        assertTrue(getHomePage().isNameFieldVisible());
        assertTrue(getHomePage().isPhoneFieldVisible());
        assertTrue(getHomePage().isTextAreaVisible());
        assertTrue(getHomePage().isSendButtonVisible());

    }

    @Test
    public void checkComponentsOnRegistrationPage() throws InterruptedException {
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        getHomePage().clickMyAccountButton();
        getHomePage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getHomePage().getMyAccountPopup());
        getHomePage().clickRegistrationButton();

        getRegistrationPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getRegistrationPage().getPageHeader());
        assertTrue(getRegistrationPage().isPageHeaderVisible());
    }

    @Test
    public void checkAddToCart() {
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        getHomePage().isSearchFieldVisible();
        getHomePage().enterTextToSearchField("maybelline");
        getSearchResultsPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME, getSearchResultsPage().getSearchProduct());
        getSearchResultsPage().clickSearchProduct();
        getProductPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getProductPage().getAddToCartButton());
        getProductPage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        assertTrue(getProductPage().isAddToCartButtonVisible());
        getProductPage().clickAddToCartButton();
        getProductPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getProductPage().getCartCount());
        assertTrue(getProductPage().isCartCountVisible());
//        getProductPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getProductPage().getCartPopup());
  //      getProductPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getProductPage().getCartPopup());
//        getProductPage().clickAddToCartButton();
//        getProductPage().clickCartButton();
//        getProductPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getProductPage().getCartPopup());
//        assertTrue(getProductPage().isCartSectionVisible());
//        getProductPage().clickAddToCartButton();
//        getProductPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME, getProductPage().getAddToCartPopupHeader());
//        assertTrue(getProductPage().isAddToCartPopupVisible());
//        getProductPage().isContinueShoppingButtonVisible();
//        getProductPage().isContinueToCartButtonVisible();
//        assertEquals(getProductPage().getAddToCartPopupHeaderText(), "You have added 1 item(s) to your cart");
//        getProductPage().clickContinueToCartButton();
//        getShoppingCartPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME, getShoppingCartPage().getShoppingCartItem());
//        getShoppingCartPage().clickCheckoutButton();
//        getShoppingCartPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME, getCheckoutPage().getPaymentCartButton());
//        getCheckoutPage().clickPaymentCartButton();
//        getShoppingCartPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME, getCheckoutPage().getPaymentForm());
//        assertTrue(getCheckoutPage().isPaymentFormVisible());
//        assertTrue(getCheckoutPage().isBillingFormVisible());
//        assertTrue(getCheckoutPage().isCompleteOrderButtonVisible());
    }
}
