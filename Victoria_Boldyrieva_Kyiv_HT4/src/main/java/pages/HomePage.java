package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(xpath = "//header")
    private WebElement header;

    @FindBy(xpath = "//footer")
    private WebElement footer;

    @FindBy(xpath = "//div[@class='header-basket empty ']")
    private WebElement cartIcon;

    @FindBy(xpath = "//form[@class='input-wrap search-wrap ng-pristine ng-valid']//input[@class='input']")
    private WebElement searchField;

    @FindBy(xpath = "//button[@class='search-button']")
    private WebElement searchButton;

    @FindBy(xpath = "//div[text()='Вход в кабинет']")
    private WebElement myAccountButton;

    @FindBy(xpath = "//form[@class='form form_auth ajax ng-pristine ng-valid active']")
    private WebElement myAccountPopup;

    @FindBy(xpath = "//input[@name='user_login']")
    private WebElement emailField;

    @FindBy(xpath = "//input[@name='user_pw']")
    private WebElement passwordField;

    @FindBy(xpath = "//button[text()='Войти']")
    private WebElement signInButton;

    @FindBy(xpath = "//a[@href='/register/']")
    private WebElement registerButton;

    @FindBy(xpath = "//div[@class='header-contact']//div[text()='Обратный звонок']")
    private WebElement callBackButton;

    @FindBy(xpath = "//form[@class='form form_callback ajax ng-pristine ng-valid active']")
    private WebElement callBackPopup;

    @FindBy(xpath = "//input[@placeholder='Ваше имя']")
    private WebElement nameField;

    @FindBy(xpath = "//input[@placeholder='Телефон']")
    private WebElement phoneField;

    @FindBy(xpath = "//textarea[@placeholder='Текст сообщения']")
    private WebElement textArea;

    @FindBy(xpath = "//button[@name='send']")
    private WebElement sendButton;



    @FindBy(xpath = "//button[@class='button nav-item js-header-toggle is-hidden-l']")
    private WebElement menuButton;

    @FindBy(xpath = "//nav[@id='main-navigation']")
    private WebElement menuPopup;




    public HomePage(WebDriver driver) {
        super(driver);
    }


    public boolean isHeaderVisible() {
        return header.isDisplayed();
    }

    public boolean isFooterVisible() {
        return footer.isDisplayed();
    }

    public boolean isCartIconVisible() {
        return cartIcon.isDisplayed();
    }

    public boolean isSearchFieldVisible() {
        return searchField.isDisplayed();
    }

    public boolean isSearchButtonVisible() {
        return searchButton.isDisplayed();
    }

    public boolean isMyAccountButtonVisible() {
        return myAccountButton.isDisplayed();
    }

    public void clickMyAccountButton() {
        myAccountButton.click();
    }

    public WebElement getMyAccountPopup(){ return myAccountPopup;}

    public boolean isEmailFieldVisible(){ return emailField.isDisplayed();}

    public boolean isPasswordFieldVisible(){ return passwordField.isDisplayed();}

    public boolean isSignInVisible(){ return signInButton.isDisplayed();}

    public boolean isRegisterVisible(){ return registerButton.isDisplayed();}
    public void clickRegistrationButton(){registerButton.click();}

    public boolean isCallBackButtonVisible(){ return callBackButton.isDisplayed();}

    public void clickCallBackButton(){  callBackButton.click();}

    public WebElement getCallBackPopup(){ return callBackPopup;}

    public boolean isNameFieldVisible(){ return nameField.isDisplayed();}

    public boolean isPhoneFieldVisible(){ return phoneField.isDisplayed();}

    public boolean isTextAreaVisible(){ return textArea.isDisplayed();}

    public boolean isSendButtonVisible(){ return sendButton.isDisplayed();}




    public void clickStoreButton() {
        menuButton.click();
    }

    public boolean isStorePopupVisible() {
        return menuPopup.isDisplayed();
    }


    public void clickSearchButton() {
        searchButton.click();
    }



    public WebElement getSearchField(){return searchField;}
    public WebElement getSearchButton(){return searchButton;}

    public void clickCartButton() {
        cartIcon.click();
    }


    public void enterTextToSearchField(final String searchText) {
        searchField.clear();
        searchField.sendKeys(searchText, Keys.ENTER);
    }



}
