package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;



public class SearchResultsPage extends BasePage{

    @FindBy(xpath = "//li[@data-id='159015']")
    private WebElement searchProduct;



    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public boolean isSearchProductVisible() {
        return searchProduct.isDisplayed();
    }

    public void clickSearchProduct() {
        searchProduct.click();
    }

    public WebElement getSearchProduct(){ return searchProduct;}
}
