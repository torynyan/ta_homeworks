package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegistrationPage extends BasePage{

    @FindBy(xpath = "//h1[@class='page-header']")
    private WebElement pageHeader;
    public RegistrationPage(WebDriver driver) {
        super(driver);
    }
    public boolean isPageHeaderVisible(){return pageHeader.isDisplayed();}
    public WebElement getPageHeader(){return pageHeader;}
}
