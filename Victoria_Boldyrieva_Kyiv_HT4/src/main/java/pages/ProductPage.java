package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductPage extends BasePage{

    @FindBy(xpath = "//div[@class='product-item__button']")
    private WebElement addToCartButton;

    @FindBy (xpath = "//div[@class='page-header' and text()='Корзина']")
    private WebElement cartCount;

    @FindBy(xpath = "//div[@class='popup__window']")
    private WebElement cartPopup;

    @FindBy(xpath = "//button[@class='btn-icon bag__close']")
    private WebElement closeButton;

    @FindBy(xpath = "//a[@class='bag__checkout btn btn--primary btn--full']")
    private WebElement checkOutButton;

    @FindBy(xpath = "//div[@class='popup-content']//div[text()='Maybelline New York Lash/Cils Sensational']")
    private WebElement cartTitleSection;

    @FindBy(xpath = "//div[@class='header-basket empty ']")
    private WebElement cartIcon;


    public ProductPage(WebDriver driver) {
        super(driver);
    }
    public void clickAddToCartButton(){ addToCartButton.click();}
    public boolean isAddToCartButtonVisible(){return addToCartButton.isDisplayed();}
    public WebElement getAddToCartButton(){return addToCartButton;}

    public String getCartCountText() {
        return cartCount.getText();
    }
    public boolean isCartCountVisible() {
        return cartCount.isDisplayed();
    }
    public WebElement getCartCount(){return cartCount;}
//
//    public void clickCartButton() {
//        addToCartButton.click();
//    }
//
//    public WebElement getCartPopup(){ return cartPopup;}
//    public WebElement getCartTitleSection(){ return cartTitleSection;}
//
//    public void clickAddToCartButton() {
//        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", addToCartButton);
//    }
//
//    public WebElement getAddToCartButton() {
//        return addToCartButton;
//    }
//
//    public boolean isAddToCartButtonVisible(){return addToCartButton.isDisplayed();}
//
//    public boolean isCartSectionVisible() {
//        return cartPopup.isDisplayed();
//    }
//
//    public void isCloseButtonVisible() {
//        closeButton.isDisplayed();
//    }
//
//    public String getCartSectionHeaderText() {
//        return cartTitleSection.getText();
//    }
//
//    public void isCheckOutButtonVisible() {
//        checkOutButton.isDisplayed();
//    }
//
//    public void clickCheckOutButton() {
//        checkOutButton.click();
//    }
//
//    public void clickCloseButton() {
//        closeButton.click();
//    }
////    public WebElement getAddToCartPopupHeader() {
////        return addToCartPopupHeader;
////    }
}
