package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CheckoutPage extends BasePage{



    @FindBy(xpath = "//div[@class='section section--shipping-address']")
    private WebElement billingForm;

    @FindBy(xpath = "//input[@id='checkout_email']")
    private WebElement emailField;

    @FindBy(xpath = "//button[@id='continue_button']")
    private WebElement completeOrderButton;

    public CheckoutPage(WebDriver driver) {
        super(driver);
    }



    public WebElement getPaymentForm() {
        return emailField;
    }

    public boolean isBillingFormVisible() {
        return billingForm.isDisplayed();
    }

    public boolean isEmailFieldVisible() {
        return emailField.isDisplayed();
    }

    public boolean isCompleteOrderButtonVisible() {
        return completeOrderButton.isDisplayed();
    }

}
