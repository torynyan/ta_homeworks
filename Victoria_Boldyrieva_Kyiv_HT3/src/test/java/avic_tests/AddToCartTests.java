package avic_tests;

import org.checkerframework.checker.units.qual.A;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;
import pages.HomePage;

import static org.testng.Assert.assertEquals;

public class AddToCartTests extends BaseTest  {
    private String EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART = "2";
    JavascriptExecutor js = (JavascriptExecutor) driver;

    @Test
    public void checkAddToCart() {

        getHomePage().waitVisibilityOfElement(500,getHomePage().getAddToCartButton());
                //js.executeScript("arguments[0].scrollIntoView();", getHomePage().getAddToCartButton());
        getHomePage().clickOnAddToCartButton();
        getHomePage().waitVisibilityOfElement(30, getHomePage().getAddToCartPopup());
        getHomePage().clickOnPlusProductButton();
        getHomePage().waitVisibilityOfElement(30, getHomePage().getAddToCartPopup());

        getHomePage().clickOnContinueShoppingButton();
        assertEquals(getHomePage().getTextOfAmountProductsInCart(), EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART);
    }
}
