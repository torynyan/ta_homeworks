package avic_tests;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SearchResultsTests extends BaseTest{

    private static final String SEARCH_KEYWORD = "Samsung Galaxy";
    private static final String EXPECTED_QUERY = "query=Samsung+Galaxy";
    private static final int EXPECTED_AMOUNT_OF_PRODUCTS= 5;

    @Test
    public void checkThatUrlContainsSearchWord()  {
        getHomePage().searchByKeyword(SEARCH_KEYWORD);

        Assert.assertTrue(getDriver().getCurrentUrl().contains(EXPECTED_QUERY));
    }

    @Test
    public void checkImageAmountOnROGPhone5Page(){
        getHomePage().clickOnAsusButton();
        getAsusPage().clickOnRogPhoneButton();

        getROGPhone5Page().implicitWait(30);

        Assert.assertEquals(getSearchResultsPage().getSearchResultsCount(), EXPECTED_AMOUNT_OF_PRODUCTS);
    }

    @Test
    public void checkThatSearchResultsContainsSearchWord(){
        getHomePage().searchByKeyword(SEARCH_KEYWORD);
        for (WebElement webElement:getSearchResultsPage().getSearchResultsList()) {
            Assert.assertTrue(webElement.getText().contains(SEARCH_KEYWORD));
        }
    }
}
