package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import static org.openqa.selenium.By.id;
import static org.openqa.selenium.By.xpath;

public class HomePage extends BasePage {
    private static final String SEARCH_INPUT = "//input[@id='input_search']";
    private static final String SEARCH_BUTTON = "//button[@class='button-reset search-btn']";
    private static final String PRODUCT_CATALOG_BUTTON = "//span[@class='sidebar-item']";
    private static final String ASUS_BUTTON = "//img[@alt='Asus' and @class=' lazyloaded']";
    private static final String AMOUNT_OF_PRODUCTS_IN_CART = "//div[contains(@class,'header-bottom__cart')]//div[contains(@class,'cart_count')]";
    private static final String ADD_TO_CART_BUTTON = "//div[@class='col-xs-12 sale-col']//div[@data-product='248874']//a[@class='prod-cart__buy']";
    private static final String ADD_TO_CART_POPUP = "js_cart";
    private static final String PLUS_PRODUCT_BUTTON = "//span[@class='js_plus btn-count btn-count--plus ']";
    private static final String CONTINUE_SHOPPING_BUTTON = "//div[@class='btns-cart-holder']//a[contains(@class,'btn--orange')]";

    //JavascriptExecutor js = (JavascriptExecutor) driver;

    public HomePage(WebDriver driver) {
        super(driver);
    }


    public void searchByKeyword(final String keyword){
        driver.findElement(By.xpath(SEARCH_INPUT)).sendKeys(keyword);
        driver.findElement(By.xpath(SEARCH_BUTTON)).click();
    }

    public void clickOnProductCatalogButton() {
        driver.findElement(xpath(PRODUCT_CATALOG_BUTTON)).click();
    }

    public void clickOnAsusButton() {
        driver.findElement(xpath(ASUS_BUTTON)).click();
    }

    public void clickOnPlusProductButton() {
        driver.findElement(xpath(PLUS_PRODUCT_BUTTON)).click();
    }

    public String getTextOfAmountProductsInCart() {
        return driver.findElement(xpath(AMOUNT_OF_PRODUCTS_IN_CART)).getText();
    }

    public void clickOnAddToCartButton() {
        driver.findElements(xpath(ADD_TO_CART_BUTTON)).get(0).click();
    }

    public void clickOnContinueShoppingButton() {
        driver.findElement(xpath(CONTINUE_SHOPPING_BUTTON)).click();
    }

    public By getAddToCartPopup() {
        return id(ADD_TO_CART_POPUP);
    }

    public By getAddToCartButton(){
        return xpath(ADD_TO_CART_BUTTON);
    }
}
