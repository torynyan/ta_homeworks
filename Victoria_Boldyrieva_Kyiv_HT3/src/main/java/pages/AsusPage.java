package pages;

import org.openqa.selenium.WebDriver;

import static org.openqa.selenium.By.xpath;

public class AsusPage extends BasePage {

    private static final String ROG_PHONE_BUTTON = "//div[contains(text(),'ASUS ROG Phone 5 Pro 16/512GB ')]";

    public AsusPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnRogPhoneButton() {
        driver.findElement(xpath(ROG_PHONE_BUTTON)).click();
    }

}
