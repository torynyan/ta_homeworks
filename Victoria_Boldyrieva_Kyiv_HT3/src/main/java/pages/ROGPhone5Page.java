package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.openqa.selenium.By.xpath;

public class ROGPhone5Page extends BasePage{
    private static final String IMAGE_GALLERY ="//div[@class=\"row no-wrap-product\"]//div[@class=\"swiper-wrapper\"]//a";

    public ROGPhone5Page(WebDriver driver) {
        super(driver);
    }

    public int getSearchResultsCount() {
        return getSearchResultsList().size();
    }

    public List<WebElement> getSearchResultsList() {
        return driver.findElements(xpath(IMAGE_GALLERY));
    }// возращаемый тип List<WebElement> потому что при помощи метода findElements мы получаем список элементов по заданному икспасу

}
