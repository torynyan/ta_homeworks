package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

import java.util.List;

import static org.openqa.selenium.By.xpath;

public class SearchResultPage extends BasePage {

    private static final String SEARCH_RESULTS_PRODUCTS_LIST="//div[@class=\"row no-wrap-product\"]//div[@class=\"swiper-wrapper\"]//a";

    public SearchResultPage(WebDriver driver) {
        super(driver);
    }

    public int getSearchResultsCount() {
        return getSearchResultsList().size();
    }

    public List<WebElement> getSearchResultsList() {
        return driver.findElements(xpath(SEARCH_RESULTS_PRODUCTS_LIST));
    }
}
