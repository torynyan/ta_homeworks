package tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SmokeTests extends BaseTest {


        private static final long DEFAULT_WAITING_TIME = 90;

        @Test
        public void checkMainComponentsOnHomePage() {
            getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
            getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
            assertTrue(getHomePage().isHeaderVisible());
            assertTrue(getHomePage().isFooterVisible());
            assertTrue(getHomePage().isSearchFieldVisible());
            assertTrue(getHomePage().isCartIconVisible());
            assertTrue(getHomePage().isMyAccountIconVisible());
            assertTrue(getHomePage().isMarketPlaceVisible());
            getHomePage().clickLanguageButton();
            getHomePage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getHomePage().getLanguagePopup());
            assertTrue(getHomePage().isLanguagePopupVisible());
            assertTrue(getHomePage().isLanguageClosePopupButtonVisible());
            getHomePage().clickLanguageClosePopupButton();
            getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
            getHomePage().moveToMyAccountIcon();
            getHomePage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getHomePage().getSignInButton());
            assertTrue(getHomePage().isRegisterButtonVisible());
            getHomePage().clickMarketPlaceButton();
            getMarketPlacePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
            getMarketPlacePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
            getMarketPlacePage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getMarketPlacePage().getMarketplaceTitle());
            getMarketPlacePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
            getMarketPlacePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
            assertTrue(getMarketPlacePage().isMarketPlaceTitleVisible());

        }

        @Test
        public void checkWishListError() throws InterruptedException {
            getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
            getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
            assertTrue(getHomePage().isHeaderVisible());
            assertTrue(getHomePage().isFooterVisible());
            assertTrue(getHomePage().isMyAccountIconVisible());
            getHomePage().moveToMyAccountIcon();
            getHomePage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getHomePage().getSignInButton());
            assertTrue(getHomePage().isRegisterButtonVisible());
            getHomePage().clickSignInButton();
            getSignInPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getSignInPage().getSignInTitle());
            getSignInPage().enterEmailToEmailField("torynyan@gmail.com");
            getSignInPage().enterPasswordToPasswordField("ZYtpyf.6789");
            getSignInPage().clickSignInButton();
            getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
            getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
            getHomePage().isCartIconVisible();

            getHomePage().isSearchFieldVisible();
            getHomePage().enterTextToSearchField("new balance");
            getHomePage().clickSearchButton();
            getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
            getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
            getSearchResultPage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
            getSearchResultPage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
            getSearchResultPage().clickWishListOnFirstProduct();
            getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
            getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
            getHomePage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getHomePage().getWishListProductsCount());
            getHomePage().clickWishListProductsCount();
            getWishListPage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
            getWishListPage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
            getWishListPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getWishListPage().getError());
            assertTrue(getWishListPage().getErrorText().contains("Ошибка"));
        }

        @Test
        public void checkAddToCart() {
            getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
            getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
            getHomePage().isSearchFieldVisible();
            getHomePage().enterTextToSearchField("24466592");
            getHomePage().clickSearchButton();
            getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
            getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
            getSearchResultPage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
            getSearchResultPage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
            getProductPage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
            getProductPage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
            getProductPage().clickAddToCartButton();
            getProductPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getProductPage().getAddToCartPopupHeader());
            assertTrue(getProductPage().isAddToCartPopupVisible());
            getProductPage().isCheckoutButtonVisible();
            getProductPage().isContinueToCartButtonVisible();
            assertEquals(getProductPage().getAddToCartPopupHeaderText(),"1 товар");
            getProductPage().clickCheckoutButton();
            getSignInPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getSignInPage().getSignInTitle());


        }

        @Test
        public void checkSignInWithValidData(){
            getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
            getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
            assertTrue(getHomePage().isHeaderVisible());
            assertTrue(getHomePage().isFooterVisible());
            assertTrue(getHomePage().isMyAccountIconVisible());
            getHomePage().moveToMyAccountIcon();
            getHomePage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getHomePage().getSignInButton());
            assertTrue(getHomePage().isRegisterButtonVisible());
            getHomePage().clickSignInButton();
            getSignInPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getSignInPage().getSignInTitle());
            getSignInPage().enterEmailToEmailField("torynyan@gmail.com");
            getSignInPage().enterPasswordToPasswordField("ZYtpyf.6789");
            getSignInPage().clickSignInButton();
            getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
            getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
            getHomePage().moveToMyAccountIcon();
            getHomePage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getHomePage().getNameAccount());
            assertTrue(getHomePage().getNameAccountText().contains("Victoria"));


        }
    }


