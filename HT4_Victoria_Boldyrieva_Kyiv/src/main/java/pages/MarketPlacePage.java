package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MarketPlacePage extends BasePage{
    @FindBy(xpath = "//h1[text()='Discover Marketplace']")
    private WebElement marketplaceTitle;
    public MarketPlacePage(WebDriver driver) {
        super(driver);
    }

    public boolean isMarketPlaceTitleVisible(){return marketplaceTitle.isDisplayed();}

    public WebElement getMarketplaceTitle(){return marketplaceTitle;}
}
