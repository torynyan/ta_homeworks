package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends BasePage{
    @FindBy(xpath = "//div[@class='title qa-title active']")
    private WebElement signInTitle;
    @FindBy(xpath = "//input[@class='qa-email-textbox']")
    private WebElement emailField;
    @FindBy(xpath = "//input[@class='qa-password-textbox']")
    private WebElement passwordField;
    @FindBy(xpath = "//input[@id='signin']")
    private WebElement signInButton;
    @FindBy(xpath = "//div[@class='title qa-title with-link']")
    private WebElement registerBlank;


    public SignInPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getSignInTitle() {
        return signInTitle;
    }



    public boolean isEmailFieldVisible() {
        return emailField.isDisplayed();
    }

    public boolean isPasswordFieldVisible() {
        return passwordField.isDisplayed();
    }

    public boolean isSignInButtonVisible() {
        return signInButton.isDisplayed();
    }

    public void clickSignInButton() {
        signInButton.click();
    }

    public void clickRegistrationBlank() {
        registerBlank.click();
    }



    public void enterEmailToEmailField(final String email) {
        emailField.clear();
        emailField.sendKeys(email);
    }

    public void enterPasswordToPasswordField(final String password) {
        passwordField.clear();
        passwordField.sendKeys(password);
    }

}
