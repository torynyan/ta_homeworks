package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class ProductPage extends BasePage{

    @FindBy(xpath = "//button[@data-test-id='add-button']")
    private WebElement addToCartButton;

    @FindBy(xpath = "//span[@data-test-id='miniBagItemCount']")
    private WebElement addToCartPopupHeader;

    @FindBy(xpath = "//a[@data-test-id='checkout-link']")
    private WebElement checkoutButton;

    @FindBy(xpath = "//a[@data-test-id='bag-link']")
    private WebElement continueToCartButton;

    @FindBy(xpath = "//select[//select[@data-id='sizeSelect']]")
    private WebElement productDropDown;
    Select selectProduct = new Select(productDropDown);

    @FindBy(xpath = "//div[@class='error-overlay is-failed']")
    private WebElement errorText;

    public ProductPage(WebDriver driver) {
        super(driver);
    }

    public void clickAddToCartButton() {
        addToCartButton.click();
    }

    public boolean isAddToCartPopupVisible() {
        return addToCartPopupHeader.isDisplayed();
    }

    public void isCheckoutButtonVisible() {
        checkoutButton.isDisplayed();
    }

    public String getAddToCartPopupHeaderText() {
        return addToCartPopupHeader.getText();
    }

    public void isContinueToCartButtonVisible() {
        continueToCartButton.isDisplayed();
    }

    public void clickCheckoutButton() {
        checkoutButton.click();
    }

    public WebElement getAddToCartPopupHeader() {
        return addToCartPopupHeader;
    }

    public void selectProduct() {
        selectProduct.selectByValue("S - Chest 38");
    }

    public WebElement getErrorText() {
        return errorText;
    }
}
