package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class RegisterPage extends BasePage{

    @FindBy(xpath = "//div[@id='register-form']")
    private WebElement registerForm;

    @FindBy(xpath = "//input[@id='FirstName']")
    private WebElement firstNameForm;
    @FindBy(xpath = "//input[@id='LastName']")
    private WebElement lastNameForm;
    @FindBy(xpath = "//input[@id='Email']")
    private WebElement emailForm;
    @FindBy(xpath = "//input[@id='Password']")
    private WebElement passwordForm;

    @FindBy(xpath = "//select[@id='BirthDay']")
    private WebElement dayForm;
    Select selectDay = new Select(dayForm);
    @FindBy(xpath = "//select[@id='BirthMonth']")
    private WebElement monthForm;
    Select selectMonth = new Select(monthForm);
    @FindBy(xpath = "//select[@id='BirthYear']")
    private WebElement yearForm;
    Select selectYear = new Select(yearForm);

    @FindBy(xpath = "//input[@id='register']")
    private WebElement signUpAsosButton;

    @FindBy(xpath = "//div[@class='_2NvMleXIr7pz6sy_33kKit']")
    private WebElement wellDoneRegistration;

    @FindBy(xpath = "//span[@class='qa-email-validation field-validation-error']//span")
    private WebElement errorText;
    public RegisterPage(WebDriver driver) {
        super(driver);
    }
    public WebElement getRegisterForm() {
        return registerForm;
    }
    public boolean isRegistrationFormVisible() {
        return registerForm.isDisplayed();
    }
    public WebElement getDayForm() {
        return dayForm;
    }


    public void enterFirstNameToFirstNameForm(final String fname) {
        firstNameForm.clear();
        firstNameForm.sendKeys(fname);
    }

    public void enterLastNameToLastNameForm(final String lname) {
        lastNameForm.clear();
        lastNameForm.sendKeys(lname);
    }

    public void enterEmailToEmailForm(final String email) {
        emailForm.clear();
        emailForm.sendKeys(email);
    }

    public void enterPasswordToPasswordForm(final String password) {
        passwordForm.clear();
        passwordForm.sendKeys(password);
    }


    public void selectDayForm() {
        selectDay.selectByValue("2");
    }

    public void selectMonthForm() {
        selectMonth.selectByVisibleText("Январь");
    }

    public void selectYearForm() {
        selectYear.selectByValue("1968");
    }

    public void clickSignUpAsosButton() {
        signUpAsosButton.click();
    }

    public boolean isWellDoneRegistrationVisible() {
        return wellDoneRegistration.isDisplayed();
    }

    public WebElement getWellDoneRegistration() {
        return wellDoneRegistration;
    }

    public String getErrorText() {
        return errorText.getText();
    }
}
