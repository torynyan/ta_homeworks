package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage{
    @FindBy(xpath = "//header")
    private WebElement header;

    @FindBy(xpath = "//footer[@class='QgXuFU4']")
    private WebElement footer;

    @FindBy(xpath = "//a[@data-testid='miniBagIcon']")
    private WebElement cartIcon;

    @FindBy(xpath = "//header//button[@data-testid='country-selector-btn']")
    private WebElement languageButton;

    @FindBy(xpath = "//div[@class='_3bZNClC']")
    private WebElement languagePopup;

    @FindBy(xpath = "//select[@id='country']")
    private WebElement languageDropDown;

    @FindBy(xpath = "//option[@value='PL']")
    private WebElement languagePoland;

    @FindBy (xpath = "//button[@data-testid='close-button']")
    private WebElement languageClosePopupButton;
    @FindBy (xpath = "//button[@data-testid='save-country-button']")
    private WebElement saveCountryButton;

    @FindBy(xpath = "//button[@data-testid='myAccountIcon']")
    private WebElement myAccountIcon;

    @FindBy(xpath = "//span[@class='tiqiyps']")
    private WebElement nameAccount;

    @FindBy(xpath = "//a[@data-testid='signin-link']")
    private WebElement signInButton;

    @FindBy(xpath = "//a[@data-testid='signup-link']")
    private WebElement registerButton;

    @FindBy(xpath = "//a[@data-testid='marketplace']")
    private WebElement marketPlaceButton;


    @FindBy(xpath = "//input[@id='chrome-search']")
    private WebElement searchField;

    @FindBy(xpath = "//button[@data-testid='search-button-inline']")
    private WebElement searchButton;

    @FindBy(xpath = "//a[@data-testid='savedItemsIcon']")
    private WebElement wishListProductsCount;

    @FindBy(xpath = "//button[@data-testid='signout-link']")
    private WebElement logOutButton;

    @FindBy(xpath = "//a[text()='О нас']")
    private WebElement aboutUsButton;



    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openHomePage(String url){driver.get(url);}

    public boolean isHeaderVisible() { return
        header.isDisplayed();
    }

    public boolean isFooterVisible() {
         return footer.isDisplayed();
    }

    public boolean isCartIconVisible() {
        return cartIcon.isDisplayed();
    }



    public boolean isMyAccountIconVisible() {
        return myAccountIcon.isDisplayed();
    }

    public void moveToMyAccountIcon() {
        myAccountIcon.click();
    }

    public boolean isNameAccountVisible(){return nameAccount.isDisplayed();}
    public WebElement getNameAccount(){return nameAccount;}

    public String getNameAccountText(){return nameAccount.getText();}

    public WebElement getSignInButton(){return signInButton;}

    public boolean isSignInButtonVisible() {
        return signInButton.isDisplayed();
    }

    public void clickSignInButton() {
        signInButton.click();
    }

    public boolean isRegisterButtonVisible() {
        return registerButton.isDisplayed();
    }

    public void clickRegisterButton(){registerButton.click();}
    public boolean isMarketPlaceVisible() {
        return marketPlaceButton.isDisplayed();
    }

    public void clickMarketPlaceButton(){
        marketPlaceButton.click();}



    public boolean isSearchFieldVisible() {
        return searchField.isDisplayed();
    }

    public void clickCartButton() {
        cartIcon.click();
    }

    public void clickLanguageButton() {
        languageButton.click();
    }

    public boolean isLanguagePopupVisible(){return languagePopup.isDisplayed();}

    public WebElement getLanguagePopup(){return languagePopup;}
    public WebElement getLanguageDropDown(){return languageDropDown;}
    public void clickLanguageDropDown(){languageDropDown.click();}
    public void clickLanguagePoland(){languagePoland.click();}
    public void clickSaveCountryButton(){saveCountryButton.click();}




    public boolean isLanguageClosePopupButtonVisible(){return languageClosePopupButton.isDisplayed();}

    public void clickLanguageClosePopupButton(){ languageClosePopupButton.click();}

    public void enterTextToSearchField(final String searchText) {
        searchField.clear();
        searchField.sendKeys(searchText);
    }

    public void clickSearchButton() {
        searchButton.click();
    }

    public String getAmountOfProductsInWishList() {
        return wishListProductsCount.getText();
    }
    public void clickWishListProductsCount(){wishListProductsCount.click();}
    public WebElement getWishListProductsCount(){ return wishListProductsCount;}

    public void clickLogOutButton(){
        logOutButton.click();
    }
    public void clickAboutUsButton(){
        aboutUsButton.click();
    }
}
