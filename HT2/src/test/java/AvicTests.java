import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import javax.swing.plaf.ListUI;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.By.xpath;
import static org.testng.Assert.*;

public class AvicTests {

    private WebDriver driver;


    @BeforeTest
    public void profileSetup(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
    }


    @BeforeMethod
    public void testsSetup(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://avic.ua/ua");
    }

    @Test
    public void checkThatUrlContainsSearchWord(){
        driver.findElement(xpath("//input[@id='input_search']")).sendKeys("Samsung Galaxy");//вводим в поиск iPhone 11
        driver.findElement(xpath("//button[@class='button-reset search-btn']")).click();
        assertTrue(driver.getCurrentUrl().contains("query=Samsung+Galaxy"));
    }

    @Test
    public void checkTitle(){
        String title = driver.getTitle();
        assertTrue(title.equals("AVIC ™ - зручний інтернет-магазин побутової техніки та електроніки в Україні. | Avic"));
    }
    @Test
    public void checkImageAmountsOnSearchPage() throws InterruptedException {
        driver.findElement(xpath("//img[@alt='Asus' and @class=' lazyloaded']")).click();
        driver.findElement(xpath("//div[contains(text(),'ASUS ROG Phone 5 Pro 16/512GB ')]")).click();



        List<WebElement> imageList = driver.findElements(xpath("//div[@class=\"row no-wrap-product\"]//div[@class=\"swiper-wrapper\"]//a"));

       int actualElementsSize = imageList.size();
        assertEquals(actualElementsSize, 5);
    }

    @Test
    public void checkAddTwoElementsToCart() throws InterruptedException {
      WebElement element = driver.findElement(xpath("//div[@class='col-xs-12 sale-col']//div[@data-product='248874']//a[@class='prod-cart__buy']"));

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", element);

        element.click();

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("js_cart")));

        driver.findElement(xpath("//span[@class='js_plus btn-count btn-count--plus ']")).click();
        Thread.sleep(5000);
        driver.findElement(xpath("//button[@title='Close' and @type]")).click();

        js.executeScript("arguments[0].scrollIntoView();", driver.findElement(xpath("//header")));

        String actualProductsCountInCart =
                driver.findElement(xpath("//div[contains(@class,'header-bottom__cart')]//div[contains(@class,'cart_count')]"))
                        .getText();


        assertEquals(actualProductsCountInCart, "2");



    }
    @AfterMethod
    public void tearDown(){
        driver.quit();
    }
}
